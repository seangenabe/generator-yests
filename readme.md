# generator-yests

Generator for a simple TypeScript npm package.

## Installation

```bash
npm i -g yo generator-yests
```

## Usage

```bash
mkdir new-project
cd new-project

yo yests
```
