<% if (testing === "ava") { -%>
import test from "ava"

test
<%_ } else if (testing === "t0") { -%>
import { test, run } from "t0"

run()
<%_ } %>
