import Generator from "yeoman-generator"
import { basename } from "path"
import globalModules from "global-modules"
import { promisify } from "util"
import sortPackageJson from "sort-package-json"

const npm = require(`${globalModules}/npm`)

const npmLoad = promisify((cb: (err: Error) => void) => npm.load(cb))

class TypeScriptGenerator extends Generator {
  props: Partial<{
    name: string
    description: string
    keywords: string[]
    author: {
      name: string
      email: string
    }
    gitlabUsername: string
    testing: "ava" | "t0"
  }> = {
    gitlabUsername: "seangenabe"
  }

  async initializing() {
    await npmLoad()
    this.props.author = {
      name: npm.config.get("init-author-name") || "",
      email: npm.config.get("init-author-email") || ""
    }
  }

  async prompting() {
    const prompts = [
      {
        name: "name",
        message: "Package name",
        default: basename(this.destinationPath())
      },
      {
        name: "description",
        message: "Description"
      },
      {
        name: "keywords",
        message: "Package keywords (comma-separated)",
        filter(words: string) {
          return words.split(/\s*,\s*/g)
        }
      },
      {
        name: "testing",
        message: "Add a testing suite",
        type: "list",
        default: false,
        choices: [
          { name: "None", value: undefined },
          { name: "AVA", value: "ava" },
          "t0"
        ]
      }
    ]
    const props = await this.prompt(prompts)
    Object.assign(this.props, props)
  }

  writing() {
    this.writePackageJson()
    this.copyFiles()
  }

  copyFiles() {
    const files = [
      ".github/FUNDING.yml",
      ".vscode/settings.json",
      ".vscode/tasks.json",
      ".gitignore.tpl",
      "readme.md",
      "license.md",
      "index.ts",
      "tsconfig.json",
      ".gitlab-ci.yml",
      ".prettierrc.yml",
      ".npmignore.tpl"
    ]
    if (this.props.testing) {
      files.push("test.ts")
    }
    for (let file of files) {
      const destName = file.replace(/\.tpl$/, "")
      this.fs.copyTpl(
        this.templatePath(file),
        this.destinationPath(destName),
        this.props
      )
    }
  }

  writePackageJson() {
    let basePkgName = this.getBasePkgName()
    let pkg: any = {
      name: this.props.name,
      version: "1.0.0",
      description: this.props.description,
      scripts: {
        build: "tsc",
        watch: "tsc --watch",
        test: `echo "Error: no test specified" && exit 1`,
        prepublishOnly: "npm run build"
      },
      homepage: `https://gitlab.com/seangenabe/${basePkgName}`,
      author: `${this.props.author!.name} <${this.props.author!.email}>`,
      bugs: {
        url: `https://gitlab.com/seangenabe/${basePkgName}/issues`
      },
      repository: `gitlab:seangenabe/${basePkgName}`,
      license: "MIT",
      main: "index.js",
      keywords: this.props.keywords || []
    }
    if (this.props.testing) {
      pkg.scripts.pretest = "npm run build"
      pkg.scripts.prepublishOnly = "npm test"
      if (this.props.testing === "ava") {
        pkg.scripts.test = "ava"
      } else {
        pkg.scripts.test = "node test"
      }
    }

    pkg = sortPackageJson(pkg)
    this.fs.writeJSON(this.destinationPath("package.json"), pkg)
  }

  getBasePkgName() {
    let basePkgName: string
    if (this.props.name!.charAt(0) === "@") {
      basePkgName = this.props.name!.split("/")[1]
    } else {
      basePkgName = this.props.name!
    }
    return basePkgName
  }

  install() {
    const toInstall = ["typescript", "@types/node"]
    if (this.props.testing) {
      toInstall.push(this.props.testing)
    }
    //this.npmInstall(toInstall, { "save-dev": true })
    this.spawnCommandSync("pnpm", ["install", "-D", ...toInstall])
    this.spawnCommandSync("git", ["init"])
    this.spawnCommandSync("git", [
      "remote",
      "add",
      "origin",
      `git@gitlab.com:${this.props.gitlabUsername}/${this.getBasePkgName()}.git`
    ])
  }
}

export = TypeScriptGenerator
